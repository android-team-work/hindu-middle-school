package com.webtunix.hindumiddleschool.usermodule.studentactivities.ui.send;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.webtunix.hindumiddleschool.R;


public class SendFragment extends Fragment {



    private SendViewModel sendViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_send, container, false);


        TextView textView = root.findViewById(R.id.text_send);



        textView.setText("Everything blossoms in its own time. The foundation of Hindu Middle School, abheypur Panchkula was laid in 1982 by the founder of the Hindu Educational Society - Late Sardar Gurdev Singh Manipur (Advocate) and General Secretary Sardar Kuldeep Singh (Advocate).The Co-education School is Recognised by the Haryana Education Department. The school receives no Government or private aid and fees are its only income. Hence, fees have to be adjusted from time to time to keep up, with the rising costs and salaries. The School is in a triple storey building having around 20 rooms. It is also furnished with a library , multi- use laboratory and a creativity room to comfort various needs of children.\n" +
                "\n" +
                "\n" +
                " Our objective is to promote total development of the students so as to be fully human, intellectually competent, morally sound, spiritually oriented, loving, caring and committed to the cause of justice and truth, being actively concerned about national development.");

            return root;
    }


}
